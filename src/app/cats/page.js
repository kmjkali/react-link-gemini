
'use client'

import { useState } from 'react'
import axios from 'axios'

export default function Cats () {
    const [cats, setCats] = useState('')
    const [response,setResponse] = useState('')

    const handleQuestion = e => {
        setCats(e.target.value)
    }
    const handleSubmit = e => {
        e.preventDefault()

        const apiUrl = 'https://generativelanguage.googleapis.com/v1/models/gemini-1.5-flash:generateContent?key=??'

        const data = {"contents": [{"parts": [{"text": cats}]}]}
        axios.post(apiUrl,data)
            .then(res => {
                setResponse(res.data.candidates[0].content.parts[0].text)
            })
            .catch(err => {console.log(err)})
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input type="text" value={cats} onChange={handleQuestion} placeholder="어떤 고양이 품종이 궁금하세요?"/>
                <button type="submit">확인</button>
            </form>
            {response && <p>{response}</p>}
        </div>
    )
}

